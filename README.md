# PROJET L014 2020

## Introduction

Synchronizer of 2 trees

Version 1.0

Authors: ClementG8 & FabienCarre

## Important

/!\ DO NOT FORGET THAT WE WORK WITH ENVIRONMENT VARIABLES /!\

## Installation

Put the project folder "TREE_SYNCRO" at the root of your personal directory
Execute ./install_syncro

## USAGE

Start the script: ". . /boot"

To turn off the synchronizer, run the script ". ./boot" and select 'OFF'.

INFO: A "syncro_log" folder contains log files concerning synchronization.
